# Multi-resolution Lid driven cavity in 3D

This example is the simulation of the famous numerical benchamrk of the three dimensional lid-driven cavity. The current example was built by combining two examples from the main repo, namely `/examples/showCases/cavity3d` and `/examples/showCases/gridRefinement3d`.

![Lid driven cavity geometry. All walls have zero velocity except for the lid.](figs/cavity_setup.png)


The top lid is driven with a velocity $\vec u=(U, 0, 0)$ while
all the other boundaries have velocity $\vec u=0$. The boundary conditions are set by the regularized BC technique. The multi-resolution geometry has 3 levels and a maximum level of 5. This leads to $2^5$ blocks at the finest level. 

## Generation of the grid density file

1. `cd palabos/examples/codesByTopic/gridRefinement/generateGridDensityFromBoxes/` 
2. Compile `generateGridDensityFromBoxes` by running the usual cmake and make commands.
3. Configure the xml file to achieve the desired block configurations.
4. Use the resultant .dat file in the `cavity3d_multires` example and properly configure its config.xml file.
5. Compile and run `cavity3d_multires`.